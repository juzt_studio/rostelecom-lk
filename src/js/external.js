/*
 * Third party
 */
// тут подключаются сторонние библиотеки (желательно минифицированные) через gulp-file-include

// 00-0-2017: author
// Используем https://github.com/kenwheeler/slick/   (slick-slider)
// Для слайдера в шапке
/** @ @include('bower_components/slick-carousel/slick/slick.js')**/

// 03-08-2017: drtvader
// Используем https://modernizr.com/
@@include('external/modernizr-custom.js')

// 07-08-2017: Malich
// Используем https://refreshless.com/nouislider/
@@include('bower_components/nouislider/distribute/nouislider.js')

// 07-08-2017: MynameisIM
@@include('bower_components/select2/dist/js/select2.min.js')

// 07-08-2017: MynameisIM
@@include('bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')

// 27-09-2017: VD
// Используем: https://github.com/nazar-pc/PickMeUp
@@include('external/pickmeup.js')