define(function() {
    return {
        //- время для анимаций
        animationTime:      500,
        animationLongTime: 1500,
        //- разрешения
        mobile:             320,
        tablet:             768,
        desktopMin:         960,
        desktop:           1440,
        desktopBig:        1280
    };
});