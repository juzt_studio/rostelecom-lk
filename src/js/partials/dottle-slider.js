//====================
// QSR: dottle-slider
// 04-08-2017: Malich
//---------------------
// Палзунок
//====================
$(document).ready(function() {
    if ($('.js-slider-range').length > 0) {
        function noUiDottleSlide(options, optionsSlider) {

            // На ходим первый элемент среди '.js-slider-range' и присваиваем, этот элемент будет нашим ползунком
            var slider = document.querySelectorAll(options.toddleSlide);

            // Создали цыкл, так как noUiSlider неможет принимать в себя массив элементов
            slider.forEach(function(element, index) {

                // Создали ползунок для элемента из массива и передали ему опции из optionsSlider
                noUiSlider.create(element, optionsSlider);

                // Перемешаем наш инпут за слайдер, и делаем это для каждого элемента отдельно
                $(element).append($(element).find(options.toddleCounter));

                // Обработка собития на палзунок, noUiSlider имеет свой .on(), также для каждого элемента свой
                element.noUiSlider.on('update', function( values, handle ) {
                    $(element).find(options.toddleCounter).val(parseInt(values[handle]));
                });

                // Стандартная обработка события input'ов
                $(element).find(options.toddleCounter).on('input', function() {
                    element.noUiSlider.set([this.value, null]);
                });
            });
        };

        noUiDottleSlide({
            toddleSlide: '.js-slider-range',
            toddleCounter: '.js-toddle-count'
        }, {
            start: 0,
            connect: [true, false],
            range: {
                'min': 0,
                'max': 20
            }
        });
    }
});