/**
* RTD: accordion
* 30-08-2017: Mell.Blimm
* ---------------------
* раскрытие списка / выбор клиента
*/
$(document).ready(function() {
    $('.js-accordion-link').on('click', function(){
        var next = $(this).next('.js-accordion-item');
        var parent = $(this).parent();
        if ($(this).hasClass('active')) {
            parent.removeClass('active');
            $(this).removeClass('active');
            next.find('.js-accordion-item.active').slideUp();
            next.find('.active').removeClass('active');
            next.removeClass('active').slideUp();
        } else {
            if (next.hasClass('group')) {
                next.children(0).addClass('active').show();
            }
            parent.addClass('active');
            $(this).addClass('active');
            next.addClass('active').slideDown();
        }
    });
    
});