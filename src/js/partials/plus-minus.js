//====================
// QSR: plus-minus
// 16-08-2017: Malich
//---------------------
// Увеличение или уменьшение числа
//====================
$(document).ready(function() {
    // Получаем число
    function getCountPlusMinus($this) {
        return parseInt($this.siblings(dot + countClass).val());
    }

    // Устонавливаем число
    function setCountPlusMinus($this, setValue) {
        $this.siblings(dot + countClass).val(setValue);
    }

    // Проверяем, есть ли у нас вообше контейнер с "+" и "-"
    if ($('.js-plus-minus-cont').length > 0) {
        // Максимальное число
        var maxCount = 10;

        // Обявляем переменны, хронят класс
        var contClass = 'js-plus-minus-cont',
            plusClass = 'js-plus',
           minusClass = 'js-minus',
           countClass = 'js-plus-minus-count',
                  dot = '.';

        // Обработка собтий
        // Обработка на +
        $(dot + plusClass).on('click', function() {
            var value = getCountPlusMinus($(this));
            if (maxCount > value) {
                value++;
                setCountPlusMinus($(this), value);
            }

            if (maxCount == value) {
                $(this).parent(dot + contClass).addClass('full');
            }
        });

        // Обработка на -
        $(dot + minusClass).on('click', function() {
            var value = getCountPlusMinus($(this));

            // Условие на уменьшение, можем уменьшить до 1
            if (value > 1) {
                value--;
                setCountPlusMinus($(this), value);
            }

            // Если максимальное число не ровно то удоляем класс с нашего контейнера
            if (maxCount != value) {
                $(this).parent(dot + contClass).removeClass('full');
            }
        });

        // Обработка ввода в инпут, где мы модем ввести 10
        $(dot + countClass).on('input', function() {
            if (parseInt($(this).val()) > maxCount) {
                $(this).val(maxCount);
            } else if (parseInt($(this).val()) < 1) {
                $(this).val(1);
            }

            if (maxCount == parseInt($(this).val())) {
                $(this).parent(dot + contClass).addClass('full');
            } else {
                $(this).parent(dot + contClass).removeClass('full');
            }
        });
    }
});