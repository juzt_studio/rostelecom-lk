//====================
// QSR: open-tooltip-choose
// 07-08-2017: Malich
//---------------------
// Открываем тултип
//====================
$(document).ready(function() {

    // Параметр который занет, на каком устройстве запушен скрипт
    var isMobile = (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) ? true : false;

    var tooltipLink = 'js-open-tooltip-choose' // Класс для ссылки
    ,  tooltipBlock = 'js-tooltip-choose' // Класс для блока
    , tooltipParent = 'js-parent-tooltip-choose'
    ,            ms = 300 // Время для fade(In/Out)
    ,   msClearTime = ms - 50 // Время для setTimeout
    , clearTimeLink // id для отмены setTimeout (ссылка)
    , clearTimeBlock; // id для отмены setTimeout (блок)


    $('.' + tooltipLink).each(function(ind, val) {
        $(this).attr('data-link-tooltip', ind);
        $(this).parent().find('.' + tooltipBlock).attr('data-target-tooltip', ind);
    });

    function permutationTooltip(outBody, $toolTip, $parentLink, callback) {
        if (outBody) {
            $('body').append($toolTip);
        } else {
            $parentLink.append($toolTip);
        }
        if (callback != undefined) callback();
    }

    function positionTooltip($toolTip, $link, callback) {
        var left = ($link.offset().left) - ($toolTip.width() / 2)
        ,    top = $link[0].getBoundingClientRect().bottom;

        $('.' + tooltipLink).removeClass('tooltip-angle');
        if (($link[0].getBoundingClientRect().bottom + $toolTip.height()) > window.innerHeight) {
            top -= ($toolTip.height() + $link[0].getBoundingClientRect().height);
            $toolTip.addClass('tooltip-angle');
        }

        $toolTip.css({
            top: top,
            left: left,
        });

        if (callback != undefined) callback();
    }

    function getTooltipForLinkAttr($link) {
        return  $('.' + tooltipBlock + '[data-target-tooltip="' + $link.attr('data-link-tooltip') + '"]');
    }

    function getLinkForTooltipAttr($toolTip) {
        return $('.' + tooltipLink + '[data-link-tooltip="' + $toolTip.attr('data-target-tooltip') + '"]');
    }

    // Функция удаляет все класс на ссылка и все блоки тултипа скрывает
    function globalRemoveClassToolTip(callback) {
        $('.' + tooltipLink).removeClass('active');
        $('.' + tooltipBlock).stop().fadeOut(ms);
        if (callback != undefined) callback();
    }

    function openTooltip($this) {
        globalRemoveClassToolTip(function() {
            $this.addClass('active');
            var block = getTooltipForLinkAttr($this);
            block.stop().fadeIn(ms);
        });
    }

    function closeTooltip($this, callback) {
        clearTimeLink = setTimeout(function() {
            $this.removeClass('active');
            var block = getTooltipForLinkAttr($this);
            block.stop().fadeOut(ms,function() {
                if (callback != undefined) callback();
            });
        }, msClearTime);
    }

    function clearIdTimeout() {
        clearTimeout(clearTimeLink);
        clearTimeout(clearTimeBlock);
    }

    // Функция показывает/скрывает блок тултипа, который лежит в томже элементе, что и ссылка
    function toggleTooltip($this, open) {
        clearIdTimeout();
        if (open) {
            // Показываем

            openTooltip($this);
        } else {

            // Скрываем
            closeTooltip($this);
        }
    }

    // Проверка на сушествование элементов
    if ($('.' + tooltipLink).length > 0 && $('.' + tooltipBlock).length > 0) {
        if (isMobile) {
            // Условие для тач устройст (мобилка, айпады)

            $(document).on('touchend', function(event) {
                if ($(event.target).hasClass(tooltipLink) || $(event.target).parents('.' + tooltipLink).hasClass(tooltipLink)) {
                    var $link = ($(event.target).hasClass(tooltipLink)) ? $(event.target) : $(event.target).parents('.' + tooltipLink);
                    if (!$link.hasClass('active')) {
                        permutationTooltip(true, getTooltipForLinkAttr($link),
                            $link.parent('.' + tooltipParent), function() {
                            positionTooltip(getTooltipForLinkAttr($link), $link, function() {
                                toggleTooltip($link, true);
                            });
                        });
                    } else {
                        clearIdTimeout();
                        closeTooltip($link, function() {
                            $('.' + tooltipBlock).each(function(ind, val) {
                                var _link = getLinkForTooltipAttr($(this));
                                permutationTooltip(false, $(this), _link.parent('.' + tooltipParent));
                            });
                            // permutationTooltip(false, getTooltipForLinkAttr($link), $link.parent('.' + tooltipParent));
                        });
                    }
                } else if (!$(event.target).hasClass(tooltipBlock)) {
                    if (!$(event.target).parents('.' + tooltipBlock).hasClass(tooltipBlock)) {
                        $('.' + tooltipLink).removeClass('active');
                        $('.' + tooltipBlock).stop().fadeOut(ms);
                    }
                }
            });

            $(document).on('touchmove', function() {
                $('.' + tooltipBlock).each(function(ind, val) {
                    var _link = getLinkForTooltipAttr($(this));
                    permutationTooltip(false, $(this), _link.parent('.' + tooltipParent));
                });
                globalRemoveClassToolTip();
            });
        } else {
            // Условие для десктопа (пк)


            $(window).on('mousewheel', function(event) {
                $('.' + tooltipBlock).each(function(ind, val) {
                    var _link = getLinkForTooltipAttr($(this));
                    permutationTooltip(false, $(this), _link.parent('.' + tooltipParent));
                });
                globalRemoveClassToolTip();
            });

            // Событие на ссылку
            $('.' + tooltipLink).hover(

                // Пришли
                function() {
                    var $this = $(this);
                    globalRemoveClassToolTip(function() {
                        $('.' + tooltipBlock).each(function(ind, val) {
                            var _link = getLinkForTooltipAttr($(this));
                            permutationTooltip(false, $(this), _link.parent('.' + tooltipParent));
                        });
                        permutationTooltip(true, getTooltipForLinkAttr($this),
                            $this.parent('.' + tooltipParent), function() {
                            positionTooltip(getTooltipForLinkAttr($this), $this, function() {
                                toggleTooltip($this, true);
                            });
                        });
                    });
                },

                // Ушли
                function() {
                    var $this = $(this);
                    clearIdTimeout();
                    closeTooltip($this, function() {
                        permutationTooltip(false, getTooltipForLinkAttr($this), $this.parent('.' + tooltipParent));
                    });
                }
            );

            // Событе на блок
            $('.' + tooltipBlock).hover(

                // Пришли
                function() {
                    clearTimeout(clearTimeLink);
                    var $this = $(this);
                    globalRemoveClassToolTip(function() {
                        var link = getLinkForTooltipAttr($this);
                        link.addClass('active');
                        $this.stop().fadeIn(ms);
                    });
                },

                // Ушли
                function() {
                    var $this = $(this);
                    clearTimeLink = setTimeout(function() {
                        var link = getLinkForTooltipAttr($this);
                        link.removeClass('active');
                        $this.fadeOut(ms, function() {
                            $('.' + tooltipBlock).each(function(ind, val) {
                                var _link = getLinkForTooltipAttr($(this));
                                permutationTooltip(false, $(this), _link.parent('.' + tooltipParent));
                            });
                        });
                    }, msClearTime);
                }
            )
        }
    }
});