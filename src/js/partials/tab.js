/**
* RTD: tab
* 04-08-2017: drtvader
* ---------------------
* табы
*/

$(document).ready(function() {

    var isMobile = (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) ? true : false;

    function getCoords(elem) { // кроме IE8-
        var box = elem.getBoundingClientRect();

        return {
            top: box.top + pageYOffset,
            left: box.left + pageXOffset
        };
    }

    // var filtering = false;

    var SetFilter = function ($this, _, isResize) {
        isResize = isResize || false;

        // if (filtering) return false;
        // filtering = true;
        var width = $this.outerWidth();
        var left = (!isMobile) ? $this.offset().left : $this[0].getBoundingClientRect().left;
        var scrollbar;

        if (_.$container != null && _.$container.length > 0) {
            scrollbar = $this.parents('.' + _.containerClass).find('.' + _.navClass);
        } else {
            scrollbar = _.$nav;
        }
        var my_left = (!isMobile) ? scrollbar.offset().left : scrollbar[0].getBoundingClientRect().left;
        var my_css_left = (!isMobile) ? parseInt(scrollbar.css('left')) : parseInt(scrollbar["0"].offsetLeft);

        scrollbar.animate({
            'left':my_css_left - (my_left-left)+'px',
            'width':width+'px'
        },400,'swing',function () {
            filtering = false;
        });

        if (!isResize) {

            var tab;
            if (_.$container != null && _.$container.length > 0) {
                tab = $this.parents('.' + _.containerClass).find('.' + _.tabClass);
            } else {
                tab = _.$tab
            }

            tab.stop().animate({
                opacity:0
            }, 300, function() {
                var tabOpen;
                $(this).css('display', 'none');
                if (_.$container != null && _.$container.length > 0) {
                    tabOpen = $this.parents('.' + _.containerClass).find('.' + _.tabClass + '[' + _.attrTabCont + '="' + $this.data('tab') + '"]');
                } else {
                    tabOpen = $('.' + _.tabClass + '[' + _.attrTabCont + '="' + $this.data('tab') + '"]');
                }

                tabOpen.css({
                    display: 'block',
                    opacity: 0
                }).stop().animate({
                    opacity: 1
                }, 300, function() {
                    if (_.$container != null && _.$container.length > 0) {
                        $this.parents('.' + _.containerClass).find('.' + _.tabClass).removeClass('active');
                        $this.parent().siblings('.active').removeClass('active');
                    } else {
                        _.$tab.removeClass('active');
                        _.$link.parent().removeClass('active');
                    }
                    $(this).addClass('active');
                    $this.parent().addClass('active');

                    // currBeginMonth = null;
                    // beginDay = null;
                    // endDay = null;
                    // beginMonth = null;
                    // endMonth = null;
                    // currEndMonth = null;
                });
            });
        }
    };

    function getFindActive($selector) {
        var activeTab;
        $selector.each(function(index, value) {
            if ($(this).parent().hasClass('active')) {
                activeTab = $(this);
            }
        });
        return activeTab;
    }

    function initTabs(options) {
        var _ = {
            $container:                   null,
            $tab:         $('.' + options.tab),
            $nav:         $('.' + options.nav),
            $link:       $('.' + options.link),
            containerClass:  options.container,
            tabClass:              options.tab,
            navClass:              options.nav,
            linkClass:            options.link,
            attrTabCont:   options.attrTabCont,
            isResize:         options.isResize,
        };

        _.$container = ("container" in options)?$('.' + options.container):null;

        if (_.$link.length > 0) {
            if (_.$container != null) {
                _.$container.each(function() {
                    SetFilter( getFindActive( $(this).find('.' + _.linkClass) ), _ );
                });
            } else {
                SetFilter( getFindActive( _.$link ), _ );
            }
        }

        _.$link.on('click',function () {
            if (!$(this).parent().hasClass('active')) {
                SetFilter( $(this), _ );
            }
        });

        if (_.isResize) {
            var timeResizeClear;
            $(window).resize(function() {
                clearTimeout(timeResizeClear);
                timeResizeClear = setTimeout(function() {
                    if (_.$link.length > 0) {
                        SetFilter( getFindActive(_.$link), _, true );
                    }
                }, 50);
            });
        }
    }

    initTabs({
        tab: 'js-tab-content',
        nav: 'js-line-nav',
        link: 'js-tab-link',
        attrTabCont: 'data-tab-content',
        isResize: true,
    });

    initTabs({
        container: 'js-open-calendar',
        tab: 'js-tab-content-calendar',
        nav: 'js-line-nav-calendar',
        link: 'js-tab-link-calendar',
        attrTabCont: 'data-tab-content',
        isResize: false,
    });
});