/**
* RTD: horisontal-scroll-popup
*04-09-2017: VD
* ---------------------
* Горизонтальный стилизованный скролл для попапа на десктопе
*/

$(document).ready(function() {
    if( $('.js-tariff-scroll').length ) {
        $('.js-tariff-scroll').perfectScrollbar({
            suppressScrollY: true
        });
    }
});