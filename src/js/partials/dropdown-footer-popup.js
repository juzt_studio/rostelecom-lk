/**
* RTD: dropdown-footer-popup
* 05-09-2017: Mell.Blimm
* ---------------------
* Раскрытие дропдауна в футерном попапе
*/

$(document).ready(function() {

    if ( $('.js-footer-dropdown--link').length ) {

        var link = $( '.js-footer-dropdown--link' ),
            duration = 300;

        link.on( 'click', function() {
            var $this = $(this),
                dropdownBlock = $('.js-footer-dropdown--popup');

            if ( !$this.hasClass('active') ) {
                $this.addClass('active');
                dropdownBlock.stop().slideDown(duration);
            } else {
                $this.removeClass('active');
                dropdownBlock.stop().slideUp(duration);
            };

        });
    };

});