/**
* RTD: open-filter-mobile
* 10-08-2017: drtvader
* ---------------------
* открытие фильтра на мобилке
*/
$(document).ready(function() {
    $('.js-open-filter').on('click', function(){
        // $('.js-filter-block').parents('b-table-sort__center').addClass('active');
        $(".js-filter-block[data-filter-block='"+($(this).attr("data-filter"))+"']").addClass('active');
        $('.b-page-wrapper').addClass('active');
    });
    // закрвыаем фильтр при клике на кнопку применить
    $('.js-close-filter').on('click', function(){
        $('.js-filter-block, .b-page-wrapper').removeClass('active');
    });

    if ( $('.js-table-select-permutation').length ) {
        var fatherConteiner = $('.js-table-sort-parent');
        var addMobile = function() {
            $('.js-table-sort-parent').each(function() {
                $(this).find('.js-after-permutation').after($(this).find('.js-table-select-permutation'));
            });
        };

        var addDesktop = function() {
            $('.js-table-sort-parent').each(function() {
                $(this).prepend($(this).find('.js-table-select-permutation'));
            });
        };

        var permutationRun = function() {

            if ( $(window).width() <= 767 ) {
                addMobile();
            } else {
                addDesktop();
            };
        };

        permutationRun();

        $( window ).resize(function() {
            permutationRun();
        });
    }
});