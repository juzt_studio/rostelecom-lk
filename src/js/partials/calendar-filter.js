$(document).ready(function() {
    // Создание Русской локализации
    pickmeup.defaults.locales['ru'] = {
        days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
    };

    function Calendar($element, settings) {
            // Объявление переменных
        var _ = this;

        _.defaults = {
            months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            classSelectedDay: 'pmu-selected',
            classCurrentDay: 'pmu-today',
            classDay: 'pmu-button',
            classHas: 'pmu-not-in-month',
            classMonth: 'pmu-month',
            beginDay: null,
            endDay: null,
            beginMonth: null,
            endMonth: null,
            currBeginMonth: null,
            currEndMonth: null,
            calendar:'js-calendar',
            one: 'js-calendar-one',
            two: 'js-calendar-two',
            areaMonth: 'js-area-month',
            areaTemporary: 'js-area-temporary',
            areaTemp: 'js-area-temp',
            beginDayClass: 'pmu-start',
            endDayClass: 'pmu-finish',
        };

        _.elements = {
            $container: $element,
            $calendar: $element.find("." + _.defaults.calendar),
            $one: $element.find("." + _.defaults.one),
            $two: $element.find("." + _.defaults.two),
            $areaMonth: $element.find("." + _.defaults.areaMonth),
            $areaMonth: $element.find("." + _.defaults.areaTemporary),
            $areaMonth: $element.find("." + _.defaults.areaTemp),
        }

        $.extend(_.defaults, settings);

        _.options = $.extend(_.defaults, _.elements);

        _.init();
    };

    Calendar.prototype.findMonth = function(strMonth) {
        var _ = this;

        for (var i = 0; i < _.options.months.length; i++) {
            if (_.options.months[i] == strMonth) {
                return i;
            }
        }
        return false;
    };

    Calendar.prototype.findElemDayForDate = function($element, number, adClasses) {
        var _ = this;

        $element.find('.' + _.options.classDay).each(function(ind, val) {
            if (parseInt($(this).text()) == number && !$(this).hasClass(_.options.classHas)) {
                $(this).addClass(adClasses);
            }
        });
    };

    Calendar.prototype.getCurrMongth = function($element) {
        var _ = this
        ,  el = $element.find('.' + _.options.classMonth).text().split(',');
        return _.findMonth(el[0]);
    };

    Calendar.prototype.daySelected = function($element) {
        var _ = this;

        $element.find('.' + _.options.classSelectedDay).removeClass(_.options.classSelectedDay);
        $element.find('.' + _.options.classDay).each(function(ind, val) {
            if (parseInt($(this).text()) >= _.options.beginDay
                && parseInt($(this).text()) <= _.options.endDay
                && !$(this).hasClass(_.options.classHas)) {
                $(this).addClass(_.options.classSelectedDay);
            }
        });
    };

    Calendar.prototype.range = function() {
        var _ = this;

        $('.' + _.options.beginDayClass).removeClass(_.options.beginDayClass);
        $('.' + _.options.endDayClass).removeClass(_.options.endDayClass);

        if (_.options.beginMonth == _.options.currBeginMonth && _.options.endMonth == _.options.currEndMonth) {
            if (_.options.beginMonth != _.options.endMonth) {
                setTimeout(function() {
                    var openSelected = false;
                    _.options.$one.find('.' + _.options.classDay).each(function(ind, val) {
                        if (parseInt($(this).text()) == _.options.beginDay && !$(this).hasClass(_.options.classHas)) {
                            openSelected = true;
                        }

                        if (openSelected) {
                            $(this).addClass(_.options.classSelectedDay);
                        }
                    });

                    openSelected = true;
                    _.options.$two.find('.' + _.options.classDay).each(function(ind, val) {
                        if (parseInt($(this).text()) >= 1 && openSelected) {
                            $(this).addClass(_.options.classSelectedDay);
                            if (parseInt($(this).text()) == _.options.endDay && !$(this).hasClass(_.options.classHas)) {
                                openSelected = false;
                            }
                        }
                    });

                    _.findElemDayForDate(_.options.$one, _.options.beginDay, _.options.beginDayClass);
                    _.findElemDayForDate(_.options.$two, _.options.endDay, _.options.endDayClass);
                }, 50);
            } else if (_.options.beginMonth == _.options.endMonth) {
                setTimeout(function() {
                    _.daySelected(_.options.$one);
                    _.daySelected(_.options.$two);
                    _.findElemDayForDate(_.options.$one, _.options.beginDay, _.options.beginDayClass);
                    _.findElemDayForDate(_.options.$one, _.options.endDay, _.options.endDayClass);
                    _.findElemDayForDate(_.options.$two, _.options.beginDay, _.options.beginDayClass);
                    _.findElemDayForDate(_.options.$two, _.options.endDay, _.options.endDayClass);
                }, 50);
            }
        } else {
            $('.' + _.options.classSelectedDay).removeClass(_.options.classSelectedDay);
        }
    };

    // Управелние чекбоксами, пример: месяца 1 область и текушее или прошлое 2 область
    Calendar.prototype.calendarChange = function($this) {
        var _ = this
        , parent;

        if ($this.parents('.' + _.options.areaMonth).hasClass(_.options.areaMonth)) {
            parent = $this.parents('.' + _.options.areaMonth);
        } else {
            parent = $this.parents('.' + _.options.areaTemporary);
        }

        if (parent.hasClass(_.options.areaMonth)) {
            $('.' + _.options.areaTemporary).find('input[type="checkbox"]').prop('checked', false);
        } else if (parent.hasClass(_.options.areaTemporary)) {
            $('.' + _.options.areaMonth).find('input[type="checkbox"]').prop('checked', false);
        }
    };

    Calendar.prototype.initSetYearSlider = function() {
        var $sliderNav = $('.js-slide-nav')
        ,     $prevNav = $sliderNav.find('.js-slide-prev')
        ,     $currNav = $sliderNav.find('.js-slide-curr')
        ,     $nextNav = $sliderNav.find('.js-slide-next')
        ,         date = new Date
        ,            _ = this;

        $('.' + _.options.classSelectedDay).removeClass(_.options.classSelectedDay);
        $('.' + _.options.classCurrentDay).removeClass(_.options.classCurrentDay);
        $prevNav.parent().css('margin-left', '-100%');
        $prevNav.text( (parseInt(date.getFullYear()) - 1) );
        $currNav.text( parseInt(date.getFullYear()) );
        $nextNav.text( (parseInt(date.getFullYear()) + 1) );
    };

    Calendar.prototype.nextMonth = function(calendar) {
        var _ = this;
        pickmeup(calendar).next();
    };

    Calendar.prototype.prevMonth = function(calendar) {
        var _ = this;
        pickmeup(calendar).prev();
    };

    Calendar.prototype.reAd = function(day, month, currMonth) {
        var _ = this;
        if (_.options[day] == 1) {
            if (_.options[month] < _.options[currMonth]) {
                _.options[month] = _.options[currMonth];
            } else if (_.options[month] == _.options[currMonth]) {
                _.options[month]++;
            }
        }
    };

    Calendar.prototype.init = function() {
        var _ = this;

        // Запуск календарей
        pickmeup(_.options.$calendar[0], {
            flat : true,
            calendars : 1,
            locale: 'ru',
        });

        pickmeup(_.options.$one[0], {
            flat : true,
            calendars : 1,
            locale: 'ru',
        });

        pickmeup(_.options.$two[0], {
            flat : true,
            calendars : 1,
            locale: 'ru',
        });

        // Для таба "Дата"
        _.options.$calendar[0].addEventListener('pickmeup-change', function(e) {
            $('.' + _.options.areaTemp).find('input[type="checkbox"]').prop('checked', false);
        });

        $('.' + _.options.areaTemp).find('input[type="checkbox"]').on('change', function() {
            setTimeout(function() {
                _.options.$calendar.find('.' + _.options.classSelectedDay).removeClass(_.options.classSelectedDay);
            }, 50);
        });

        /* Для таба "Период" */
        // Событие для начала периода
        _.options.$one[0].addEventListener('pickmeup-change', function(e) {
            _.options.beginDay = e.detail.date.getDate();
            _.options.beginMonth = e.detail.date.getUTCMonth();
            _.options.currBeginMonth = _.getCurrMongth(_.options.$one);

            _.reAd('beginDay', 'beginMonth', 'currBeginMonth');

            if (_.options.beginMonth > _.options.currBeginMonth) {
                _.nextMonth(_.options.$one[0]);
            } else if (_.options.beginMonth < _.options.currBeginMonth) {
                _.prevMonth(_.options.$one[0]);
            }

            _.options.beginMonth = e.detail.date.getUTCMonth();
            _.options.currBeginMonth = _.getCurrMongth(_.options.$one);

            _.reAd('beginDay', 'beginMonth', 'currBeginMonth');

            _.options.$one.find('.' + _.options.classSelectedDay).removeClass(_.options.classSelectedDay);
            if (_.options.endDay != null && _.options.endMonth != null) {
                _.range();
            }
        });

        // Событие для конечного периода
        _.options.$two[0].addEventListener('pickmeup-change', function(e) {
            _.options.endDay = e.detail.date.getDate();
            _.options.endMonth = e.detail.date.getUTCMonth();
            _.options.currEndMonth = _.getCurrMongth(_.options.$two);

            _.reAd('endDay', 'endMonth', 'currEndMonth');

            if (_.options.endMonth > _.options.currEndMonth) {
                _.nextMonth(_.options.$two[0]);
            } else if (_.options.endMonth < _.options.currEndMonth) {
                _.prevMonth(_.options.$two[0]);
            }

            _.options.endMonth = e.detail.date.getUTCMonth();
            _.options.currEndMonth = _.getCurrMongth(_.options.$two);

            _.reAd('endDay', 'endMonth', 'currEndMonth');

            _.options.$two.find('.' + _.options.classSelectedDay).removeClass(_.options.classSelectedDay);
            if (_.options.beginDay != null && _.options.beginMonth != null) {
                _.range();
            }
        });

        $('.' + _.options.areaMonth + ', .' + _.options.areaTemporary)
            .find('input[type="checkbox"]').on('change', function() {
            _.calendarChange($(this));
        });

        _.initSetYearSlider();
    };

    $('.js-open-calendar').each(function() {
        $(this).calendar = new Calendar($(this), {});
    });

/*-----------------------=Слайдер по годам=---------------------------*/

    function testDate(nextDate) {
        var date = new Date;

        if (parseInt(date.getFullYear()) >= parseInt(nextDate)) {
            return true;
        } else {
            return false;
        }
    }

    function moveSlide(one, two, three, step) {
        one.text(two.text());

        var curr = parseInt(three.text());
        two.text(three.text()).parent();

        curr += step ;
        three.text(curr);
    }

    var isAnimation = true;
    $('.js-slide-move').on('click', function(e) {
        e.preventDefault();

        if (isAnimation) {
            isAnimation = false;
            var marginDefaultLeft = '-100%'
            , $nav = $(this).parents('.js-slider-cont').find('.js-slide-nav')
            , move = $(this).data('move')
            , moveElemText;

            if (move == 'right') {
                marginDefaultLeft = '-200%';
                moveElemText = $nav.find('.js-slide-next').text();
            }

            if (move == 'left') {
                marginDefaultLeft = '0%';
                moveElemText = $nav.find('.js-slide-prev').text();
            }

            if (testDate(moveElemText)) {
                $nav.animate({
                    marginLeft: marginDefaultLeft
                }, 500, function() {
                    var $prev = $(this).find('.js-slide-prev')
                    ,   $curr = $(this).find('.js-slide-curr')
                    ,   $next = $(this).find('.js-slide-next');

                    if (move == 'right') {
                        moveSlide($prev, $curr, $next, 1);
                    }

                    if (move == 'left') {
                        moveSlide($next, $curr, $prev, -1);
                    }

                    $(this).css('margin-left', '-100%');
                    isAnimation = true;
                });
            } else {
                isAnimation = true;
            }
        }
    });
});