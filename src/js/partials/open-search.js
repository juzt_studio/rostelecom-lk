/**
* RTD: open-search
* 07-08-2017: drtvader
* ---------------------
* открытие поиска
*/
$(document).ready(function() {
  $('.js-open-search').bind('click', function(){
    $('.b-table-sort__hidden-search').addClass('active');
  });

  $('.js-clear').bind('click', function(){
    $('.b-table-sort__hidden-search').removeClass('active');
  });
});