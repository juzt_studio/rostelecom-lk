//====================
// RTD: open-popup
// 17-08-2017: Malich
//---------------------
// Открываем попап
//====================
function popup(options) {
  var link = options.link || false,
     popup = options.popup || false,
     close = options.close || false,
     shadow = options.shadow || 'js-popup-shadow',
  outClose = options.outClose || false;

  var elementLink = $('.' + options.link) || false,
     elementPopup = $('.' + options.popup) || false,
     elementClose = $('.' + options.close) || false,
           fadeMs = options.fadeMs || 300;

  elementLink.on('click', function() {
    elementPopup.stop().fadeIn(fadeMs);
  });

  elementClose.on('click', function() {
    elementPopup.stop().fadeOut(fadeMs);
  });

  $(document).on('keydown', function(e) {
    if (e.keyCode == 27) {
      elementPopup.stop().fadeOut(fadeMs);
    }
  });

  if (outClose) {
    $(document).on('click', function(event) {
      if ($(event.target).hasClass(shadow)) {
        elementPopup.stop().fadeOut(fadeMs);
      }
    });
  }
}

$(document).ready(function() {
  popup({
    link: 'js-popup-link',
    popup: 'js-popup-container',
    close: 'js-popup-close',
    outClose: false,
    fadeMs: 450
  });
});