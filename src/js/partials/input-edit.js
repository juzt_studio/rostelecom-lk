/**
* RTD: input-edit
* 15-10-2017: VD
* ---------------------
* Открываем\закрываем поле ввода для редактирования названия документа
*/

$(document).ready(function() {
    if ( $( '.js-input-edit-button' ).length ) {

        var button = $( '.js-input-edit-button' );
        var parent;
        var content;
        var inputEdit;

        button.on( 'click', function() {
            parent = $(this).parent( '.js-input-edit-parent' );

            // скрываем всё содержимое в блоке
            content = parent.children();
            content.hide();

            inputEdit = parent.find('.js-input-edit');
            inputEdit.css('display', 'flex');
        });

        var changeStage = function() {
            if ( inputEdit != undefined ) {
                if (inputEdit.find('input').is(':focus')) {
                    content.show();
                    inputEdit.hide();
                };
            };
        };

        var changeStageConditions = function(elem) {
            if ( inputEdit != undefined ) {
                if ( !inputEdit.is(elem.target) 
                    && inputEdit.has(elem.target).length === 0 
                    && !button.is(elem.target) ) {

                    content.show();
                    inputEdit.hide();
                };
            };
        };

        $(document).on( {

            click: function(e) {
                changeStageConditions(e);
            },

            keyup: function(e) {
                if (e.keyCode === 13) {
                    changeStage();
                }
            }
        });
    };
});