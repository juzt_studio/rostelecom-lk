//====================
// RTD: radio-button
// 30-08-2017: Malich
//---------------------
// Открытие блоков по классам тыкая в radio button
//====================
$(document).ready(function() {

    // Анимация высоты, открытого блока
    function setHeightIfOpenAinimate() {
        if ($(".js-retractable-block.active").length > 0) {
            var height = $('.js-retractable-block.active').find('.js-retractable-wrapper').height();
            $('.js-tab-content.active').stop().animate({
                height: height
            }, 300);
        }
    }

    // Анимация fade, где $elemFadeOut - закрываем, $elemFadeIn - открываем, msFade - милисекунды
    function animateFadeBlocks($elemFadeOut, $elemFadeIn, msFade) {
        $elemFadeOut.stop().animate({
            opacity:0
        }, msFade, function() {
            $(this).css('display', 'none');
            $elemFadeIn.css({
                display: 'flex',
                opacity: 0
            }).stop().animate({
                opacity: 1
            }, msFade, function() {
                setHeightIfOpenAinimate();
            });
        });
    }

    // Проверка на наши радио
    if ($('#once-toggle').length > 0) {

        // Создаем функцию, чтобы не копировать код.
        // Здесь мы проверяем, кто унас checked, а кто нет
        function fedeInBlockRadio() {
            if ($idInputFirst.prop('checked')) {
                animateFadeBlocks($classBlockSecond, $classBlockFirst, msFade);
            } else if ($idInputSecond.prop('checked')) {
               animateFadeBlocks($classBlockFirst, $classBlockSecond, msFade);
            }
        }

        // Объявляем переменные
        var $idInputFirst     = $('#once-toggle'),
            $idInputSecond    = $('#forever-toggle'),
            $classBlockFirst  = $('.js-one-time-document'),
            $classBlockSecond = $('.js-create-subscription'),
            msFade           = 300;

        // Зпускаем функцию, котороя сразуже скрое лишьний блок
        fedeInBlockRadio();

        // Обрабатываем событи левого radio, событие изменения или change, radio меняется с true на false и наоборот
        $idInputFirst.on("change", function() {
            fedeInBlockRadio();
        });

        // Тоже событие, что и выше только для правого radio
        $idInputSecond.on("change", function() {
            fedeInBlockRadio();
        });
    }
});