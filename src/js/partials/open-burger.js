/*
* RTD: open-burger
* 07-08-2017: amedomary
* ---------------------
* открытие главного меню в шапке
*/

function toggleBurger() {
  if ($(".b-global-wrapper").hasClass("opened")) {
    $('.b-global-wrapper').removeClass('opened');
    $('.b-page-wrapper').removeClass('opened');
  } else {
    $('.b-global-wrapper').addClass('opened');
    $('.b-page-wrapper').addClass('opened');
  }
}

var clearTimeScrollMenu;

$(document).ready(function() {
  $('.js-open-menu').bind('click', function() {
    if (window.scrollY > 0) {
      $("html, body").stop().animate({scrollTop: 0}, '325', function() {
        clearTimeout(clearTimeScrollMenu);
        clearTimeScrollMenu = setTimeout(function() {
          toggleBurger();
        }, 50);
      });
    } else {
      toggleBurger();
    }
  });
});