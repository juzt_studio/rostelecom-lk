/**
* RTD: open-dropdown-block
* 11-08-2017: drtvader
* ---------------------
* открытие выпадашки
*/
$(document).ready(function() {

    function getNormalHeight($this) {
        var height;
        if ($this.parents('.b-dropdown-block').find('.js-hidden-dropdown').data('height') === undefined) {
            $this.parents('.b-dropdown-block').find('.js-hidden-dropdown').css("display", 'block');
            height = $this.parents('.b-dropdown-block')
                .find('.js-hidden-dropdown .b-dropdown-block__hidden-wrapper').outerHeight(true);
            if (height > 0) {
                $this.parents('.b-dropdown-block').find('.js-hidden-dropdown').css("display", 'none');
            }
            $this.parents('.b-dropdown-block').find('.js-hidden-dropdown').attr('data-height', height);
        } else {
            height = $this.parents('.b-dropdown-block').find('.js-hidden-dropdown').data('height');
        }
        return height;
    }

    var msSlideDown = 400;
    $('.js-open-dropdown').bind('click', function() {
        if ($(this).parents('.b-dropdown-block').find('.js-hidden-dropdown').hasClass('active')) {
            if ($('.js-retractable-block').length > 0) {
                var height = $(this).parents('.js-tab-content').height();
                height -= getNormalHeight($(this));
                $(this).parents('.js-tab-content').animate({
                    height: height
                }, msSlideDown);
            }
            $(this).parents('.b-dropdown-block').find('.js-hidden-dropdown').removeClass('active').slideUp(msSlideDown);
            $(this).removeClass('active');
        } else {
            if ($('.js-retractable-block').length > 0) {
                var height = getNormalHeight($(this));
                height += $(this).parents('.js-tab-content').height();
                $(this).parents('.js-tab-content').animate({
                    height: height
                }, msSlideDown);
            }
            $(this).parents('.b-dropdown-block').find('.js-hidden-dropdown').addClass('active').slideDown(msSlideDown);
            $(this).addClass('active');
        }
    });
});