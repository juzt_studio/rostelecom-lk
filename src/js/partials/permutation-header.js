//====================
// QSR: permutation-header
// 07-08-2017: Malich
//---------------------
// Перенос блоков для хедера
//====================
$(document).ready(function() {
    function permutationHeader() {
        var containerTop = '.js-premutation-container-top',
            containerBot = '.js-premutation-container-bot',
           headerWrapper = '.js-premutation-header-wrapper',
                    link = '.js-premutation-link',
                listInfo = '.js-permutation-list-info',
                  button = '.js-premutation-button',
              burgerMenu = '.js-premutation-burger-menu';

        if (window.innerWidth >= 320 && window.innerWidth < 768) {
            $(containerTop).append($(link));
            $(burgerMenu).append($(headerWrapper));
            $(burgerMenu).append($(listInfo));
            $(burgerMenu).append($(button));
        } else if (window.innerWidth >= 768) {
            $(containerBot).append($(link));
            $(containerBot).append($(listInfo));
            $(containerBot).append($(button));
            $(containerTop).append($(headerWrapper));
        }
    }
    permutationHeader();

    $(window).resize(function() {
        permutationHeader();
    });
});