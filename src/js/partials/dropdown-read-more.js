/**
* RTD: dropdown-read-more
* 05-09-2017: VD
* ---------------------
* Кнопка "подробнее" выпадающий блок.
*/

$(document).ready(function() {

    if ( $('.js-tariff-read-more').length ) {

        var link = $( '.js-tariff-read-more' ),
            duration = 300;

        link.on( 'click', function() {
            var $this = $(this),
                item = $this.closest('.js-tariff-item'),
                dropdownBlock = item.next('.js-tariff-dropdown');

            if ( !$this.hasClass('active') ) {
                $this.addClass('active');
                dropdownBlock.stop().slideDown(duration);
            } else {
                $this.removeClass('active');
                dropdownBlock.stop().slideUp(duration);
            };

        });
    };

});