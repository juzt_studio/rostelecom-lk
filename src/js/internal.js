(function($) {
    /**
     * глобальные переменные, которые используются многократно
     */
    var global = {
        // время для анимаций
        animationTime:      200,
        animationLongTime: 1500,

        // проверка на ios
        isIos: navigator.userAgent.match(/(iPod|iPhone|iPad)/)
    };

    /**
     * Подключение js partials
     * Example: (dog)(dog)include('partials/default.js')
     */

    /**
    * Project: name script
    * 10-04-2017: Author
    * ---------------------
    * description
    */
    /**@ @ include('partials/header-slider.js')**/

    /**
    * RTD: tab
    * 04-08-2017: drtvader
    * ---------------------
    * табы
    */
    @@include('partials/tab.js')

    /**
    * RTD: retractable-block
    * 07-08-2017: drtvader
    * ---------------------
    * выплывающий блок в табах
    */
    @@include('partials/retractable-block.js')

    /**
    * RTD: dottle-slider
    * 04-08-2017: Malich
    * ---------------------
    * Палзунок
    */
    @@include('partials/dottle-slider.js')

    /**
    * RTD: permutation-header
    * 07-08-2017: Malich
    * ---------------------
    * Перенос блоков для хедера
    */
    @@include('partials/permutation-header.js')

    /**
    * RTD: form-select
    * 07-08-2017: MynameisIM
    * ---------------------
    * Плагин для селекта
    */
    @@include('partials/select.js')

    /**
    * RTD: Scroll-for-select
    * 07-08-2017: MynameisIM
    * ---------------------
    * Прокрутка элементов select'а
    */
    @@include('partials/p-scrollbar.js')

    /*
    * RTD: open-search
    * 07-08-2017: drtvader
    * ---------------------
    * открытие поиска
    */
    @@include('partials/open-search.js')

    /**
    * RTD: open-tooltip
    * 07-08-2017: Malich
    * ---------------------
    * Открываем тултип
    */
    @@include('partials/open-tooltip.js')

    /**
    * RTD: open-burger
    * 07-08-2017: Amedomary
    * ---------------------
    * Открываем бургер меню в хедере
    */
    @@include('partials/open-burger.js')

    /**
    * RTD: tiny-select
    * 07-08-2017: Amedomary
    * ---------------------
    * селект2 для выбора формата
    */
    @@include('partials/tiny-select.js')

    /*
    * RTD: doc-type-select
    * 07-08-2017: Amedomary
    * ---------------------
    * селект2 для выбора типа документа
    */
    @@include('partials/select-type-doc.js')
    
    /*
    * RTD: select-numbers
    * 04-09-2017: MynameisIM
    * ---------------------
    * селект2 для выбора вида поиска
    */
    @@include('partials/select-numbers.js')

    /**
    * RTD: open-filter
    * 10-08-2017: drtvader
    * ---------------------
    * Открываем фильтр на мобилке
    */
    @@include('partials/open-filter.js')

    /**
    * RTD: plus-minus
    * 16-08-2017: Malich
    * ---------------------
    * Увеличение или уменьшение числа
    */
    @@include('partials/plus-minus.js')

    /**
    * RTD: open-popup
    * 17-08-2017: Malich
    * ---------------------
    * Открываем попап
    */
    @@include('partials/open-popup.js')
    /**
    * RTD: open-dropdown-block
    * 11-08-2017: drtvader
    * ---------------------
    * открытие выпадашки
    */
    @@include('partials/open-dropdown-block.js')
    /**
    * RTD: menu-dropdown
    * 22-08-2017: VD
    * ---------------------
    * В навигационном меню выпадашка
    */
    @@include('partials/menu-dropdown.js')
    /**
    * RTD: take-out-filter
    * 24-08-2017: VD
    * ---------------------
    *  Круговая диаграмма
    */
    @@include('partials/circle-statistics.js')

    /**
    * RTD: accordion
    * 30-08-2017: Mell.Blimm
    * ---------------------
    * раскрытие списка / выбор клиента
    */
    @@include('partials/accordion.js')
    /**
    * RTD: radio-button
    * 30-08-2017: Malich
    * ---------------------
    *  Открытие блоков по классам тыкая в radio button
    */
    @@include('partials/radio-button.js')

    /**
    * RTD: horisontal-scroll-popup
    *04-09-2017: VD
    * ---------------------
    * Горизонтальный стилизованный скролл для попапа на десктопе
    */
    @@include('partials/horisontal-scroll-popup.js')

    /**
    * RTD: dropdown-read-more
    * 05-09-2017: VD
    * ---------------------
    * Кнопка "подробнее" выпадающий блок.
    */
    @@include('partials/dropdown-read-more.js')

    /**
    * RTD: dropdown-footer-popup
    * 05-09-2017: Mell.Blimm
    * ---------------------
    * Раскрытие дропдауна в футерном попапе
    */
    @@include('partials/dropdown-footer-popup.js')

    /**
    * RTD: calendar-filter
    * 27-09-2017: VD
    * ---------------------
    * Календарь - фильтр дат.
    */
    @@include('partials/calendar-filter.js')

    /**
    * RTD: calendar-open
    * 03-10-2017: Malich
    * ---------------------
    * Открываем календарь
    */
    @@include('partials/calendar-open.js')

    /**
    * RTD: open-tooltip-choose
    * 07-08-2017: Malich
    * ---------------------
    * Открываем тултип, но перед эти перкидываем его за body
    */
    @@include('partials/open-tooltip-choose.js')

    /**
    * RTD: input-edit
    * 15-10-2017: VD
    * ---------------------
    * Открываем\закрываем поле ввода для редактирования названия документа
    */
    @@include('partials/input-edit.js')

})(jQuery);